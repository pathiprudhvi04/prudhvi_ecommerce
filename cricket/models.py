
from django.db import models
# Create your models here.

class Team(models.Model):
    name = models.CharField(max_length=100)

    def str(self):
        return self.name

class Player(models.Model):
    name = models.CharField(max_length=100)
    runs = models.IntegerField()
    balls_faced = models.IntegerField()
    fours = models.IntegerField()
    sixes = models.IntegerField()
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def str(self):
        return f'{self.team.name} - {self.name}'
    
class Match(models.Model):
    first_inning = models.ForeignKey(Team, on_delete=models.CASCADE)
    second_inning = models.ForeignKey(Player, on_delete=models.CASCADE)
    winner = models.CharField(max_length=50)
    
    def str(self):
        return f'{self.first_inning} vs {self.second_inning}'