from django.shortcuts import render,redirect
from .models import emp,Emp_attendance
# Create your views here.

def emp_list(request):
    # read
    emp_obj = emp.objects.all()
    return render(request, 'office/emp_list.html', 
                  {'emps': emp_obj})

# C for create

def add_emp(request):
    if request.method == 'GET':
        return render(request, 'office/add_emp.html')
    else:
        name_ = request.POST['name']
        emp_id_ = request.POST['emp_id']
        salary_ = request.POST['salary']

        s = emp(
            name = name_,
            emp_id = emp_id_,
            salary = salary_
        )
        s.save()
        return redirect('emp_list')
    
    #D for DELETE
def delete_emp(request, id):
    s = emp.objects.get(id=id)
    s.delete()
    return redirect('emp_list')

#R for READ    
def emp_detail(request, id):
    s = emp.objects.get(id=id)
    a=Emp_attendance.objects.filter(emp=s)
    return render(request, 'office/emp_detail.html',{"emp":s,"attendance":a})
#U for UPDATE
def update_emp(request, id):
    if request.method == 'GET':
        s = emp.objects.get(id=id)
        editing = True
        context = {'emp': s, 'editing': editing}
        return render(request, 'office/emp_detail.html', context)

    if request.method == 'POST':
        s = emp.objects.get(id=id)
        s.name = request.POST['name']
        s.salary = request.POST['salary']
        s.emp_id = request.POST['emp_id']
        s.save()
        return redirect('emp_detail', id)



