from django.db import models

# Create your models here.

class emp(models.Model):
    name = models.CharField(max_length = 100)
    emp_id = models.CharField(max_length = 10)
    salary = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name} - {self.emp_id} - {self.salary}'
    

class Emp_attendance(models.Model):
    e_date = models.DateField()
    emp_attendance=models.CharField(max_length=100)
    empl = models.ForeignKey(emp, on_delete=models.CASCADE)

    def __str__(self):
        return f'(Roll){self.emp.emp_id} - {self.e_date} - {self.emp_attendance}'
