from django.urls import path
from . import views
urlpatterns =[
    path('all', views.emp_list, name="emp_list"),
    path('add', views.add_emp, name='add_emp'),
    path('detail/<int:id>', views.emp_detail, name='emp_detail'),
    path('delete/<int:id>', views.delete_emp, name='delete_emp'),
    path('edit/<int:id>', views.update_emp, name='update_emp'),

]